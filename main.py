#!/usr/bin/env python

# simple program to convert a csv truth table to an equation
#does not simplify the equation
#should be easy to add or remove inputs

#Made by SammyP6

import csv
data = list(csv.reader(open("test2.csv")))
name = "O="
for x in range(1,17):
    if int(data[x][4])==1:
        if int(data[x][0])==1:
            name+= "A"
        else:
            name+="~A"
        if int(data[x][1])==1:
            name+= "B"
        else:
            name+="~B"
        if int(data[x][2])==1:
            name+="C"
        else:
            name+="~C"
        if int(data[x][3])==1:
            name+= "D"
        else:
            name+="~D"
        name+= " + "
print(name)
